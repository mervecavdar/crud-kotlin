package com.mervecavdar

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Application

fun main(args: Array<String>) {
    val startTime = System.nanoTime()
    runApplication<Application>(*args)
    val endTime = System.nanoTime()
    println("COMPILE TIME -> " + (endTime - startTime))
}
