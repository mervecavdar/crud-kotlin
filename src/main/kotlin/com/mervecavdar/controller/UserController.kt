package com.mervecavdar.controller

import com.mervecavdar.dto.UserDTO
import com.mervecavdar.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/user")
class UserController(private val userService: UserService) {

    @GetMapping("/getAll")
    fun getAll(): ResponseEntity<List<UserDTO>> = ResponseEntity.ok(userService.getAll())

    @GetMapping("/getUserById/{id}")
    fun getUserById(@PathVariable id: Long): ResponseEntity<UserDTO> = ResponseEntity.ok(userService.getUserById(id))

    @PostMapping("/save")
    fun save(@RequestBody userDTO: UserDTO): ResponseEntity<Long> = ResponseEntity.ok(userService.save(userDTO))

    @PostMapping("/update}")
    fun update(@RequestBody userDTO: UserDTO): ResponseEntity<Long> = ResponseEntity.ok(userService.update(userDTO))

    @DeleteMapping("/delete/{id}")
    fun delete(@PathVariable id: Long): ResponseEntity<Void> {
        userService.delete(id)
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build()
    }

}