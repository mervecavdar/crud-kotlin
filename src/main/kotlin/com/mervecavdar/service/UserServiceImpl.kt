package com.mervecavdar.service

import com.mervecavdar.dto.UserDTO
import com.mervecavdar.repository.UserRepository
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(private val userRepository: UserRepository) : UserService {

    override fun getAll(): List<UserDTO> =
        userRepository.findAll().asSequence().map { item -> UserDTO.toUserDto(item) }.toList()

    override fun getUserById(id: Long): UserDTO = UserDTO.toUserDto(userRepository.findById(id).get())

    override fun save(userDTO: UserDTO): Long = userRepository.save(UserDTO.toUser(userDTO)).id

    override fun update(userDTO: UserDTO): Long {
        if (!userRepository.findById(userDTO.id).isPresent) {
            throw IllegalArgumentException()
        }
        return userRepository.save(UserDTO.toUser(userDTO)).id
    }

    override fun delete(id: Long) = userRepository.delete(userRepository.findById(id).get())

}