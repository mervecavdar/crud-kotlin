package com.mervecavdar.service

import com.mervecavdar.dto.UserDTO

interface UserService {

    fun getAll(): List<UserDTO>

    fun getUserById(id: Long): UserDTO

    fun save(userDTO: UserDTO): Long

    fun update(userDTO: UserDTO): Long

    fun delete(id: Long)

}