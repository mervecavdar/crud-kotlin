package com.mervecavdar.dto

import com.mervecavdar.entity.User

data class UserDTO(val id: Long, val username: String, val name: String, val surname: String) {

    companion object {
        fun toUserDto(user: User): UserDTO {
            return UserDTO(user.id, user.username, user.name, user.surname)
        }

        fun toUser(userDTO: UserDTO): User {
            return User(userDTO.id, userDTO.username, userDTO.name, userDTO.surname)
        }
    }

}