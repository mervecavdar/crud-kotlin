package com.mervecavdar.entity

import javax.persistence.*

@Entity
@Table(name = "USER")
data class User(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Long,
    @Column val username: String,
    @Column val name: String,
    @Column val surname: String
) {

}